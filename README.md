# Adding custom payment methods
# UR
Даний модуль дозволяє додати кастомні способи оплати в системі Prestashop в необмеженій кількості.
# RU
Данный модуль позволяет добавить кастомные способы оплаты в системе Prestashop в неограниченном количестве.
# EN
This module allows you to add custom payment methods in the Prestashop system in an unlimited number.
